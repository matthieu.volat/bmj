#include <stddef.h>
#include <math.h>

static inline double
cn(	const double *signal,
	const double *reference,
	size_t n_samples_ref,
	size_t sig_idx,
	double den_ref)
{
	double num = 0, den_sig = 0;

	for (size_t i = 0; i < n_samples_ref; i++) {
		double sign = signal[i+sig_idx-n_samples_ref/2];
		num += sign * reference[i];
		den_sig += sign * sign;
	}

	return num / sqrt(den_sig * den_ref);
}

void
corrc_norm(	double *Fcc, 
			const double *signal, size_t n_samples_sig,
			const double *reference, size_t n_samples_ref)
			
{
	double den_ref = 0.0;

    for (size_t i = 0; i < n_samples_ref; i++) {
        den_ref += reference[i] * reference[i];
    } 

	#pragma omp parallel for
	for (size_t i = n_samples_ref/2; i < n_samples_sig - n_samples_ref/2; i++) {
		Fcc[i] = cn(signal, reference, n_samples_ref, i, den_ref);
	}
}
