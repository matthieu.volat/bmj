#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np

from bmj import corrc_norm

sig = np.random.rand(1000000, 1)
ref = np.random.rand(1000, 1)

x = corrc_norm(sig, ref)
