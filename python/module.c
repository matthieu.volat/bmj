#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

extern void
corrc_norm( double *Fcc,
			const double *signal, size_t n_samples_sig,
			const double *reference, size_t n_samples_ref);

static PyObject *
_corrc_norm(PyObject *self, PyObject *args)
{
	PyObject *sig_obj = NULL, *ref_obj = NULL, *Fcc_obj = NULL;
	PyArrayObject *sig_arr = NULL, *ref_arr = NULL, *Fcc_arr = NULL;

	npy_intp *sig_dims, *ref_dims;
	const double *sig_data, *ref_data;
	double *Fcc_data;

	(void)self;

	/* Extract arrays from parameters */
	if (!PyArg_ParseTuple(args, "OO", &sig_obj, &ref_obj)) {
		return NULL;
	}
	sig_arr = (PyArrayObject *)PyArray_FROM_OTF(	sig_obj, 
													NPY_FLOAT64,
													NPY_ARRAY_IN_ARRAY);
	ref_arr = (PyArrayObject *)PyArray_FROM_OTF(	ref_obj, 
													NPY_FLOAT64,
													NPY_ARRAY_IN_ARRAY);

	/* Get data pointers & dimensions */
	sig_data = PyArray_DATA(sig_arr);
	sig_dims = PyArray_DIMS(sig_arr);
	ref_data = PyArray_DATA(ref_arr);
	ref_dims = PyArray_DIMS(ref_arr);

	/* Create output array */
	Fcc_obj = PyArray_SimpleNew(1, sig_dims, NPY_FLOAT64);
	if (Fcc_obj == NULL)
		goto error;
	Fcc_arr = (PyArrayObject *)PyArray_FROM_OTF(	Fcc_obj,
													NPY_FLOAT64,
													NPY_ARRAY_OUT_ARRAY);
	Fcc_data = PyArray_DATA(Fcc_arr);

	/* Call corrc_norm function */
	memset(Fcc_data, 0, ref_dims[0]/2*sizeof(float));
	memset(Fcc_data+(sig_dims[0]-ref_dims[0]/2), 0, ref_dims[0]/2*sizeof(float));
	corrc_norm(Fcc_data, sig_data, sig_dims[0], ref_data, ref_dims[0]);

	/* Cleanup & return */
	Py_DECREF(sig_arr);
	Py_DECREF(ref_arr);
    Py_DECREF(Fcc_arr);
	Py_INCREF(Fcc_obj);
	return Fcc_obj;

error:
	Py_XDECREF(Fcc_arr);
    return NULL;
}


static PyMethodDef bmj_methods[] = {
    {"corrc_norm",
			_corrc_norm,
			METH_VARARGS,
			"Normalized correlation"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef moduledef = {
        PyModuleDef_HEAD_INIT,
        "bmj",
        NULL,
        -1,
        bmj_methods,
		NULL,
		NULL,
		NULL,
		NULL
};

PyMODINIT_FUNC
PyInit_bmj(void)
{
	PyObject *module;

	if ((module = PyModule_Create(&moduledef)) == NULL) {
		return NULL;
	}

	import_array();

	return module;
}
