if(NUMPY_INCLUDE_DIRS)
  set(NUMPY_FIND_QUIETLY TRUE)
endif(NUMPY_INCLUDE_DIRS)

find_package(PythonInterp REQUIRED)

execute_process(
    COMMAND "${PYTHON_EXECUTABLE}" -c
            "from __future__ import print_function\ntry: import numpy; print(numpy.get_include(), end='')\nexcept:pass\n"
    OUTPUT_VARIABLE NUMPY_INCLUDE_DIRS)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(
    NUMPY DEFAULT_MSG
    NUMPY_INCLUDE_DIRS)
MARK_AS_ADVANCED(NUMPY_INCLUDE_DIRS)
