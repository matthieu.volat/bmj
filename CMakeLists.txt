cmake_minimum_required(VERSION 3.0)
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake_modules)

project(bmj C)

include(cflags.cmake)
include(buildtype.cmake)

find_package(OpenMP)

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${bmj_BINARY_DIR})

enable_testing()
add_subdirectory(python)
#add_subdirectory(matlab)
