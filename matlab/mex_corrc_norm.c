#include "mex.h"
#include "math.h"
#include "matrix.h"

extern void
corrc_norm( double *Fcc,
			const double *signal, size_t n_sample_sig,
			const double *reference, size_t n_samples_ref);

void mexFunction(	int nOutputs, mxArray *ptrOutputs[],
					int nInputs, const mxArray *ptrInputs[])
{
	size_t n_samples_sig, n_samples_ref, n_samples_out;
	double *signal, *reference; /* input */
	double *Fcc; /* output */
	mwSize dims[2];

	/* check for good number of inputs/outputs */
	/*
	 * if (nInputs != 2)
	 * mexErrMsgIdAndTxt("Matlab:corrc_norm.c",
	 * "Two inputs required.");
	 * if (nOutputs != 1)
	 * mexErrMsgIdAndTxt("Matlab:corrc_norm.c",
	 * "One output required.");
	 */

	/* read in inputs */
	signal = (double*)mxGetData(ptrInputs[0]);
	reference = (double*)mxGetData(ptrInputs[1]);

	n_samples_sig = mxGetNumberOfElements(ptrInputs[0]);
	n_samples_ref = mxGetNumberOfElements(ptrInputs[1]);
	n_samples_out = n_samples_sig; /* n_samples_sig - n_samples_ref + 1; */


	/* prepare outputs */
	dims[0] = n_samples_out;
	dims[1] = 1;
	ptrOutputs[0] = mxCreateNumericArray(2,dims,mxDOUBLE_CLASS,mxREAL);
	Fcc = (double *)mxGetData(ptrOutputs[0]);

	/* call corrc_norm function */
	corrc_norm(Fcc, signal, n_samples_sig, reference, n_samples_ref);
}
